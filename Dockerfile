FROM debian:buster-slim as base
# Install our PGP key and add HTTPS support for APT
RUN apt-get update && apt-get install -qq curl
RUN curl -fsSL --remote-name https://pkg.switch.ch/switchaai/debian/dists/buster/main/binary-all/misc/switchaai-apt-source_1.0.0_all.deb
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get update && apt-get install -y apache2 nodejs dirmngr gnupg && \
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7 && \
  apt-get install -y apt-transport-https ca-certificates && \
  # Add our APT repository
  sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger buster main > /etc/apt/sources.list.d/passenger.list' && \
  apt-get update && \
  # Install Passenger
  apt-get install -y libapache2-mod-passenger && \
  passenger-config validate-install && \
  a2enmod passenger
# https://github.com/geerlingguy/ansible-role-java/issues/64
RUN apt-get install -y ./switchaai-apt-source_1.0.0_all.deb && \
  apt-get update && \
  apt-get install -y --no-install-recommends shibboleth && \
  shibd -t && \
  rm switch*
RUN mkdir /usr/share/man/man1/ && \
  DEBIAN_FRONTEND=noninteractive apt-get install -y pdftk && \
  apt-get full-upgrade && \
  apt-get autoremove

STOPSIGNAL SIGWINCH

COPY httpd-foreground /usr/local/bin/

EXPOSE 80
CMD ["httpd-foreground"]
